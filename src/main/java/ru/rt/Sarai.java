package ru.rt;

import java.util.Random;
import java.util.Stack;

public class Sarai {

    int N, M;
    int F;
    Random random;
    boolean[][] map;
    int[] heights;
    int[] L;
    int[] R;

    public Sarai(int n, int m, int f, int seed) {
        random = new Random(seed);
        this.N = n;
        this.M = m;
        this.F = f;
        map = new boolean[N][M];
        heights = new int[N];
        L = new int[N];
        R = new int[N];
        rand();
//        if(N <= 10) print();

    }

    private void print() {
        for (int y = 0; y < M; y++) {
            for (int x = 0; x < N; x++) {
                System.out.print(map[x][y] ? "# " : ". ");
            }
            System.out.println();
        }
    }

    private void rand() {
        for (int y = 0; y < M; y++) {
            for (int x = 0; x < N; x++) {
                map[x][y] = random.nextInt(F) == 0;
            }
        }
    }

    public int start1() {
        int max = 0;
        for (int y = 0; y < M; y++) {
            for (int x = 0; x < N; x++) {
                int sq = getSquare(x, y);
                if (max < sq) {
                    max = sq;
                }
            }
        }
        return max;
    }

    private int getSquare(int x, int y) {
        int max = 0;
        int minHeght = N;
        for (int width = 1; x + width - 1 < N; width++) {
            int height = getHeight(x + width - 1, y, minHeght);
            if (minHeght > height) {
                minHeght = height;
            }
            if (minHeght == 0) break;
            int sq = width * minHeght;
            if (max < sq) {
                max = sq;
            }
        }
        return max;
    }

    private int getHeight(int x, int y, int min) {
        int height = 0;
        while (height < min && y - height >= 0 && !map[x][y - height]) {
            height++;
        }
        return height;
    }

    public int start2() {
        int max = 0;
        for (int y = 0; y < M; y++) {
            getHeights(y);
            for (int x = 0; x < N; x++) {
                int sq = getSquare2(x, y);
                if (max < sq) {
                    max = sq;
                }
            }
        }
        return max;
    }

    private void getHeights(int y) {
        for (int x = 0; x < N; x++) {
            if (map[x][y]) {
                heights[x] = 0;
            } else {
                heights[x]++;
            }

        }
    }

    private int getSquare2(int x, int y) {
        int max = 0;
        int minHeght = N;
        for (int width = 1; x + width - 1 < N; width++) {
            int height = heights[x + width - 1];
            if (minHeght > height) {
                minHeght = height;
            }
            if (minHeght == 0) break;
            int sq = width * minHeght;
            if (max < sq) {
                max = sq;
            }
        }
        return max;
    }

    public int start3() {
        int max = 0;
        for (int y = 0; y < M; y++) {
            getHeights(y);
            getRights();
            getLefts();
            for (int x = 0; x < N; x++) {
                int height = heights[x];
                int width = R[x] - L[x] + 1;
                int sq = width * height;
                if (max < sq) {
                    max = sq;
                }
            }
        }
        return max;
    }

    private void getRights() {
        Stack<Integer> stack = new Stack<>();
        for (int x = 0; x < N; x++) {
            while (!stack.empty()) {
                if (heights[stack.peek()] > heights[x]) {
                    R[stack.pop()] = x - 1;
                } else {
                    break;
                }
            }
            stack.push(x);
        }
        while (!stack.empty()) {
            R[stack.pop()] = N - 1;
        }
    }

    private void getLefts() {
        Stack<Integer> stack = new Stack<>();
        for (int x = N - 1; x >= 0; x--) {
            while (!stack.empty()) {
                if (heights[stack.peek()] > heights[x]) {
                    L[stack.pop()] = x + 1;
                } else {
                    break;
                }
            }
            stack.push(x);
        }
        while (!stack.empty()) {
            L[stack.pop()] = 0;
        }
    }


}
